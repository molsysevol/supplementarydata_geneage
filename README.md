This project includes all supplementary files for our manuscript "Strong evidence for the adaptive walk model of gene evolution in Drosophila and Arabidopsis". It includes all data tables and scripts that can be used to reproduce our analyses. 

The folder "Co-factors_continuous" contains all data tables for the combined analyses wwith gene age and protein length, gene expression, RSA, protein intrinsic disorder and Grantham's physicochemical distances, including the values of each co-factor for each gene (S2-S5 Data) and the 100 bootstrap replicates for each estimate of protein sequence evolution(omega, omega_a and omega_na - S6-S9 Data in the folder "grapes_analysis")
The folder "Data" contains the full data for Drosophila (S31) and Arabidopsis (S32 Data).
The folder "Evalue" contains the table with the analysis of gene age before and after correcting for the E-value estimates (S23-S24 Data).
The folder "Function" contains the table with the analysis of gene age with protein function (S25-S26 Data).
The folder "GranthamsDistance" contains the supplementary data and code to reproduce the analysis with Grantham's distance (S27-S28 Data)
The folder "IntraClassVariation" contains the data and scripts needed to control for the variation within each category of the co-factors analysed (S14-S18).
The folder "MKRegression" contains the data sets used to run the program MK-regression (Huang 2022, MBE; S19-S22 Data).
The folder "PPI" contains the table with the data on protein-protein interactions for each protein pair in each clade (S29 Data).
The folder "RMarkdowns" has all the R scripts used to do the plots and statistical analyses for the manuscript (Files S1-S9)
The folder "SFiles" contains the supplementary files (Files S1-S9). 
The folder "Stats" contains the tables with the statistical results for the combined analyses with gene age and each co-factor (protein length, gene expression, RSA and protein disorder; S10-S13 Data).
The folder "PiN_PiS" contains the table needed to reproduce the analysis of the correlation between w_a and w_na (S30 Data).
The file "S1_Data.csv" has the results of the analysis of gene age alone, including the 100 bootstrap replicates for each estimate of protein sequence evolution(omega, omega_a and omega_na).
